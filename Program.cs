﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace danske_bandomoji
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Program started");
            //List<List<char>> inputData = fillData();
            List<List<char>> inputData = readData();

            Console.WriteLine("Start computing dependencies for letters..");
            DependenciesExtractor extractor = new DependenciesExtractor();
            List<List<char>> dependencies = extractor.findFormattedDependencies(inputData);

            Console.WriteLine("List of dependency sets:");
            foreach (var list in dependencies){
                 list.ForEach(Console.Write);
                 Console.WriteLine();
            }
            Console.WriteLine("Program ended.");
        }

        static private List<List<char>> readData(){
            Console.WriteLine("Input data:");
            List<List<char>> data = new List<List<char>>();

            string input;
            while ((input = Console.ReadLine()) != null && input != "") {
                input = input.Replace(" ", "").Replace("\t", "");

                if (!Regex.IsMatch(input, @"^[a-zA-Z]+$")){
                    Console.WriteLine("Please enter a valid string (Only letters, space and tab allowed).");
                    Console.WriteLine("Try improved line again:");
                    continue;
                }

                if (input.Count(x => x == input[0]) > 1){
                    Console.WriteLine("Incorrect input. Letter cannot depend on itself.");
                    Console.WriteLine("Try improved line again:");
                    continue;
                }

                if (data.Find(x => x[0] == input[0]) != null){
                    Console.WriteLine("Dependencies list already contains dependencies for " + input[0]);
                    Console.WriteLine("Add other dependencies:");
                    continue;
                }

                data.Add(new List<char>(input.ToCharArray(0, input.Length)));
            }
             
            return data;
        }

        static private List<List<char>> fillData(){
            List<List<char>> data = new List<List<char>>();

            data.Add(new List<char>{'A', 'B', 'C'});
            data.Add(new List<char>{'B', 'C', 'E'});
            data.Add(new List<char>{'C', 'G'});
            data.Add(new List<char>{'D', 'A', 'F'});
            data.Add(new List<char>{'E', 'F'});
            data.Add(new List<char>{'F', 'H'});

            return data;
        }
    }

}
