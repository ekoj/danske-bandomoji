using System;
using System.Collections.Generic;
using System.Linq;

namespace danske_bandomoji
{
    class DependenciesExtractor
    {
        private List<List<char>> transitiveDependencies;

        public List<List<char>> findFormattedDependencies(List<List<char>> transitiveDependencies){
            List<List<char>> dependencies = findDependencies(transitiveDependencies);
            List<List<char>> foramttedDependencies = new List<List<char>>();

            foreach (List<char> line in dependencies){
                List<char> formattedLine = new List<char>();

                foreach(char symbol in line){
                    formattedLine.Add(symbol);
                    formattedLine.Add(' ');
                }

                foramttedDependencies.Add(formattedLine);
            }
            return foramttedDependencies;
        }

        public List<List<char>> findDependencies(List<List<char>> transitiveDependencies){
            this.transitiveDependencies = transitiveDependencies;
            List<List<char>> dependencies = new List<List<char>>();

            foreach (var currTransDependencies in this.transitiveDependencies){
                List<char> extendedDependencies = findDependenciesForOneLine(currTransDependencies).Distinct().ToList();
                extendedDependencies.Sort();

                List<char> fullDependencies = new List<char>();
                fullDependencies.Add(currTransDependencies[0]);
                fullDependencies.Add(' ');
                fullDependencies.AddRange(extendedDependencies);
                dependencies.Add(fullDependencies);
            }
            
            return dependencies;
        }

        private List<char> findDependenciesForOneLine(List<char> initialDependencies){
            List<char> dependencies = new List<char>(); 

            foreach (char currentLetter in initialDependencies.Skip(1)){
                dependencies.Add(currentLetter);

                List<char> currentDependencies = this.transitiveDependencies.Find(item => item[0].Equals(currentLetter));
                if (currentDependencies != null){
                    findDependenciesForOneLine(currentDependencies);
                    dependencies.AddRange(findDependenciesForOneLine(currentDependencies));
                }
            }

            return dependencies;
        }

    }
}
